#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

LOCAL_PATH := vendor/extra

# ADB
ifneq (eng,$(TARGET_BUILD_VARIANT))
PRODUCT_SYSTEM_PROPERTIES += \
    persist.sys.usb.config=adb,mtp

PRODUCT_PROPERTY_OVERRIDES += \
    ro.adb.secure=0
endif

# Overlays
DEVICE_PACKAGE_OVERLAYS += \
    $(LOCAL_PATH)/overlay

# SEPolicy
ifneq (user,$(TARGET_BUILD_VARIANT))
SELINUX_IGNORE_NEVERALLOWS := true
endif

PRODUCT_PACKAGES += \
    init.safailnet.rc

# Soong Namespaces
PRODUCT_SOONG_NAMESPACES += \
    vendor/extra
